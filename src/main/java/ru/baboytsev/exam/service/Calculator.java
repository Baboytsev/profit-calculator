package ru.baboytsev.exam.service;

import ru.baboytsev.exam.exception.CalculationException;
import ru.baboytsev.exam.exception.NoDataException;
import ru.baboytsev.exam.model.InputData;

public interface Calculator {

    /**
     * Расчет прибыли (убытка).
     * Курс валюты берется из внешнего источника.
     *
     * @param inputData - входные данные для расчета.
     * @throws CalculationException - если при выполнении расчета произошла ошибка, плюс обертка для ошибок нижележащего ПО.
     * @throws NoDataException      - если внешний источник не вернул данные о курсе валюты.
     */
    void calculate(InputData inputData) throws CalculationException, NoDataException;
}
