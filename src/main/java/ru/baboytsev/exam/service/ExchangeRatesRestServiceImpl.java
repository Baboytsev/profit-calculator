package ru.baboytsev.exam.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.baboytsev.exam.exception.RestException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ExchangeRatesRestServiceImpl implements ExchangeRatesRestService {
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    private static final String LATEST_RATES_URI = "https://api.exchangeratesapi.io/latest?base=USD&symbols=RUB";
    private static final String RATES_FOR_DATE_URI = "https://api.exchangeratesapi.io/history?start_at={start}&end_at={end}&base=USD&symbols=RUB";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private RestTemplate restTemplate;

    public ExchangeRatesRestServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Double getLatestUsdToRubRate() throws RestException {
        try {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(LATEST_RATES_URI, String.class);
            String responseJsonString = responseEntity.getBody();
            try {
                Map<String, Object> jsonMap = OBJECT_MAPPER.readValue(responseJsonString, new TypeReference<Map<String, Object>>() {
                });
                Map<String, Double> rates = (Map<String, Double>) jsonMap.get("rates");
                return rates.get("RUB");
            } catch (IOException e) {
                throw new RestException("Can not deserialize json object " + responseJsonString);
            }
        } catch (RestClientException e) {
            throw new RestException("Can not get information from service", e);
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public Double getUsdToRubRateForDate(Date date) throws RestException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date startDate = calendar.getTime();
        String dateString = df.format(date);
        Map<String, String> parameters = new HashMap<>();
        parameters.put("start", df.format(startDate));
        parameters.put("end", dateString);
        try {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(RATES_FOR_DATE_URI, String.class, parameters);
            String responseJsonString = responseEntity.getBody();
            try {
                Map<String, Object> jsonMap = OBJECT_MAPPER.readValue(responseJsonString, new TypeReference<Map<String, Object>>() {
                });
                Map<String, Object> ratesByDate = (Map<String, Object>) jsonMap.get("rates");
                Map<String, Double> rate = (Map<String, Double>) ratesByDate.get(dateString);
                if (rate != null) {
                    return rate.get("RUB");
                } else {
                    return null;
                }
            } catch (IOException e) {
                throw new RestException("Can not deserialize json object " + responseJsonString);
            }
        } catch (RestClientException e) {
            throw new RestException("Can not get information from service", e);
        }
    }
}
