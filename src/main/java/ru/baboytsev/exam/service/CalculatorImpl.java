package ru.baboytsev.exam.service;

import ru.baboytsev.exam.exception.CalculationException;
import ru.baboytsev.exam.exception.NoDataException;
import ru.baboytsev.exam.exception.RestException;
import ru.baboytsev.exam.model.InputData;

public class CalculatorImpl implements Calculator {
    private static final double spread = 0.5;

    private ExchangeRatesRestService exchangeRatesRestService;

    public CalculatorImpl(ExchangeRatesRestService exchangeRatesRestService) {
        this.exchangeRatesRestService = exchangeRatesRestService;
    }

    @Override
    public void calculate(InputData inputData) throws CalculationException, NoDataException {
        try {
            Double latestRate = exchangeRatesRestService.getLatestUsdToRubRate();
            Double rateForDate = exchangeRatesRestService.getUsdToRubRateForDate(inputData.getDate());

            if (latestRate == null || rateForDate == null) {
                throw new NoDataException("No data");
            }
            Double result = (latestRate * inputData.getAmountUsd()) - (rateForDate * inputData.getAmountUsd()) - ((latestRate * spread) / 100);
            inputData.setProfit(result);
        } catch (RestException e) {
            throw new CalculationException("Can not get information form rest api.", e);
        }
    }
}
