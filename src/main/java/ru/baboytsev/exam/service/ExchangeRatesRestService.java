package ru.baboytsev.exam.service;

import ru.baboytsev.exam.exception.RestException;

import java.util.Date;

public interface ExchangeRatesRestService {

    /**
     * Получить курс доллара по отношению к рублю на текущую дату.
     *
     * @return - курс доллара
     * @throws RestException - если при обращении к внешнему сервису произошла ошибка или не смогли распарсить ответ.
     */
    Double getLatestUsdToRubRate() throws RestException;

    /**
     * Получить курс доллара по отношению к рублю на указанную дату.
     *
     * @param date - дата, за которую нужно получить курс доллара
     * @return - курс доллара
     * @throws RestException - если при обращении к внешнему сервису произошла ошибка или не смогли распарсить ответ.
     */
    Double getUsdToRubRateForDate(Date date) throws RestException;

}
