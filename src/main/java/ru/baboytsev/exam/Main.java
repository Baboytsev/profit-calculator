package ru.baboytsev.exam;

import org.springframework.web.client.RestTemplate;
import ru.baboytsev.exam.controller.CalculatorController;
import ru.baboytsev.exam.service.Calculator;
import ru.baboytsev.exam.service.CalculatorImpl;
import ru.baboytsev.exam.service.ExchangeRatesRestService;
import ru.baboytsev.exam.service.ExchangeRatesRestServiceImpl;
import ru.baboytsev.exam.view.CalculatorView;

public class Main {

    public static void main(String[] args) {

        RestTemplate restTemplate = new RestTemplate();
        ExchangeRatesRestService exchangeRatesRestService = new ExchangeRatesRestServiceImpl(restTemplate);

        Calculator calculator = new CalculatorImpl(exchangeRatesRestService);

        CalculatorView calculatorView = new CalculatorView();
        CalculatorController calculatorController = new CalculatorController(calculatorView, calculator);
        calculatorController.bind();

        calculatorView.setVisible(true);


//        JFrame mainFrame = new JFrame("Exchange rates calculator");
//        mainFrame.setSize(400,400);
//        mainFrame.addWindowListener(new WindowAdapter() {
//            public void windowClosing(WindowEvent windowEvent){
//                System.exit(0);
//            }
//        });
//        mainFrame.add(calculatorView);
//        mainFrame.setVisible(true);
    }
}
