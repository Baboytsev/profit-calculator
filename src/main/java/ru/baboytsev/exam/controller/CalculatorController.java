package ru.baboytsev.exam.controller;

import ru.baboytsev.exam.exception.CalculationException;
import ru.baboytsev.exam.exception.NoDataException;
import ru.baboytsev.exam.model.InputData;
import ru.baboytsev.exam.service.Calculator;
import ru.baboytsev.exam.view.CalculatorView;

import javax.swing.*;

public class CalculatorController {

    private CalculatorView view;

    private Calculator calculator;

    public CalculatorController(CalculatorView view, Calculator calculator) {
        this.view = view;
        this.calculator = calculator;
    }

    public void bind() {
        view.getRecalculateButton().addActionListener(e -> {
            InputData inputData = new InputData();
            try {
                view.getData(inputData);
                if (valid(inputData)) {
                    calculator.calculate(inputData);
                    view.setData(inputData);
                } else {
                    JOptionPane.showMessageDialog(new JFrame(), "Заполните, пожалуйста, дату покупки валюты и сумму.", "Предупреждение", JOptionPane.WARNING_MESSAGE);
                }
            } catch (CalculationException e1) {
                e1.printStackTrace();
                JOptionPane.showMessageDialog(new JFrame(), "Не удалось выполнить рассчет", "Ошибка", JOptionPane.ERROR_MESSAGE);
            } catch (NoDataException e1) {
                e1.printStackTrace();
                JOptionPane.showMessageDialog(new JFrame(), "Нет данных о курсе валюты на заданную дату", "Ошибка", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private boolean valid(InputData inputData) {
        return inputData.getDate() != null && inputData.getAmountUsd() != null;
    }
}
