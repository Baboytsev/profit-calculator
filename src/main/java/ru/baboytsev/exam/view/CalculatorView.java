package ru.baboytsev.exam.view;

import org.springframework.util.StringUtils;
import ru.baboytsev.exam.model.InputData;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CalculatorView extends JFrame {
    private static final DateFormat df = new SimpleDateFormat("dd.MM.yyyy");


    private JPanel jPanel;

    private JLabel dateLabel;
    private JLabel amountUsdLabel;
    private JFormattedTextField amountUsd;
    private JFormattedTextField date;
    private JButton recalculateButton;
    private JFormattedTextField profit;


    public CalculatorView() {
        setSize(600, 400);
        setContentPane(jPanel);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            MaskFormatter maskFormatter = new MaskFormatter("##.##.####");
            maskFormatter.install(date);

            NumberFormat nf = NumberFormat.getNumberInstance();
            nf.setMaximumFractionDigits(2);
            NumberFormatter nff = new NumberFormatter(nf);
            DefaultFormatterFactory factory = new DefaultFormatterFactory(nff);
            amountUsd.setFormatterFactory(factory);

            NumberFormat nfRub = NumberFormat.getNumberInstance();
            nfRub.setMaximumFractionDigits(2);
            NumberFormatter nffRub = new NumberFormatter(nfRub);
            DefaultFormatterFactory factoryRub = new DefaultFormatterFactory(nffRub);
            profit.setFormatterFactory(factoryRub);

        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }
    }


    public void setData(InputData data) {
        if (data.getDate() != null) {
            date.setText(df.format(data.getDate()));
        } else {
            date.setValue(null);
        }
        if (data.getAmountUsd() != null) {
            amountUsd.setValue(data.getAmountUsd());
        } else {
            amountUsd.setValue(null);
        }
        if (data.getProfit() != null) {
            profit.setValue(data.getProfit());
        } else {
            profit.setValue(null);
        }
    }

    public void getData(InputData data) {
        if (!StringUtils.isEmpty(date.getText())) {
            try {
                data.setDate(df.parse(date.getText()));
            } catch (ParseException e) {
                //если люда попали, значит дату не ввели и к нам пришел шаблон
                data.setDate(null);
            }
        } else {
            data.setDate(null);
        }
        if (amountUsd.getValue() != null) {
            Object value = amountUsd.getValue();
            if (value instanceof Long) {
                data.setAmountUsd(((Long) value).doubleValue());
            }
            if (value instanceof Double) {
                data.setAmountUsd((Double) value);
            }
        } else {
            data.setAmountUsd(null);
        }
        if (profit.getValue() != null) {
            data.setProfit((Double) profit.getValue());
        } else {
            data.setProfit(null);
        }
    }

    public JButton getRecalculateButton() {
        return recalculateButton;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        jPanel = new JPanel();
        jPanel.setLayout(new GridBagLayout());
        jPanel.setMaximumSize(new Dimension(90, 200));
        jPanel.setMinimumSize(new Dimension(200, 90));
        dateLabel = new JLabel();
        dateLabel.setText("Дата покупки валюты");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(0, 10, 0, 0);
        jPanel.add(dateLabel, gbc);
        amountUsdLabel = new JLabel();
        amountUsdLabel.setText("Сумма валюты в долларах");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(0, 10, 0, 0);
        jPanel.add(amountUsdLabel, gbc);
        date = new JFormattedTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        jPanel.add(date, gbc);
        amountUsd = new JFormattedTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        jPanel.add(amountUsd, gbc);
        recalculateButton = new JButton();
        recalculateButton.setMargin(new Insets(0, 0, 0, 0));
        recalculateButton.setText("Рассчитать прибыль (убыток)");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        jPanel.add(recalculateButton, gbc);
        profit = new JFormattedTextField();
        profit.setEditable(false);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        jPanel.add(profit, gbc);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return jPanel;
    }
}
